# Controlo de Versões

* https://bitbucket.org/1211711/insis-bpmn/src/development/

# Ficheiro e caminho destino

* dynamicFormGen - C:\Program Files\WSO2\Enterprise Integrator\6.6.0\wso2\business-process\repository\deployment\server\jaggeryapps\bpmn-explorer\assets
* actions.js - C:\Program Files\WSO2\Enterprise Integrator\6.6.0\wso2\business-process\repository\deployment\server\jaggeryapps\bpmn-explorer\js
* activiti - C:\Program Files\WSO2\Enterprise Integrator\6.6.0\wso2\business-process\conf
* configuracao - C:\Program Files\WSO2\Enterprise Integrator\6.6.0\wso2\business-process\conf

# Utilizadores

* criador
* admin (Convidado)
* bruno (Convidado)
* teles (Convidado)
* fornecedor1 (Fornecedor)
* fornecedor2 (Fornecedor)
* fornecedor3 (Fornecedor)
* fornecedor4 (Fornecedor)

# Bibliografia

* Moodle INSIS - https://moodle.isep.ipp.pt/course/view.php?id=1805
* Tutoriais wso2 - https://docs.wso2.com/display/EI660/BPMN+Tutorials+
* Mocks - https://designer.mocky.io/manage
* Activiti Blog - https://hub.alfresco.com/t5/alfresco-process-services/bg-p/blog-BPM-software?_ga=2.44101281.462854879.1649024081-548898405.1647038471
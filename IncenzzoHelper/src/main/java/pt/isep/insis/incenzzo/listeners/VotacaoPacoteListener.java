package pt.isep.insis.incenzzo.listeners;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class VotacaoPacoteListener implements TaskListener {

	private static final long serialVersionUID = 1L;

	private static final String CONVIDADO = "convidado";
	private static final String ACEITA_PACOTE = "aceitaPacote";
	private static final String CONVIDADOS_AFIRMATIVOS = "convidadosAfirmativos";

	private static Log log = LogFactory.getLog(VotacaoPacoteListener.class);

	@Override
	public void notify(DelegateTask executionContext) {
		log.info("A recolher resultados da votação de convidados...");

		List<String> convidadosAfirmativos = (executionContext.hasVariable(CONVIDADOS_AFIRMATIVOS))
				? (List<String>) executionContext.getVariable(CONVIDADOS_AFIRMATIVOS)
				: new ArrayList<String>();

		String respostaConvidado = executionContext.getVariable(ACEITA_PACOTE, String.class);
		String convidado = executionContext.getVariable(CONVIDADO, String.class);

		log.info("Convidado: " + convidado + "\nResposta: " + respostaConvidado);

		if (respostaConvidado.equals("sim"))
			convidadosAfirmativos.add(convidado);

		executionContext.setVariable(CONVIDADOS_AFIRMATIVOS, convidadosAfirmativos);

	}

}

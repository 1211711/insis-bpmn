package pt.isep.insis.incenzzo.complex.type;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.activiti.engine.form.AbstractFormType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class MultiSelect extends AbstractFormType {

	private static final long serialVersionUID = 1L;
	
	public static final String TYPE_NAME = "multiSelect";
	
	private static Log log = LogFactory.getLog(MultiSelect.class);
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return TYPE_NAME;
	}
	
	@Override
	public Object convertFormValueToModelValue(String propertyValue) {
		log.info("convert FORM VALUE to MODEL VALUE");
		log.info(propertyValue);
		
		return propertyValue;
	}

	@Override
	public String convertModelValueToFormValue(Object modelValue) {
		log.info("convert MODEL VALUE to FORM VALUE");
		
		if (modelValue == null) {
			log.info("Object is null");
			return null;
		}
		
		if (modelValue instanceof ArrayList) {
			System.out.println(modelValue.toString());
			return modelValue.toString();
		}
		
		List<JsonNode> list = new ArrayList<JsonNode>();
		Iterator<JsonNode> nodes = ((ArrayNode) modelValue).elements();
		
		while (nodes.hasNext()) {
			list.add(nodes.next());
		}
			
		return list.toString();
	}

}

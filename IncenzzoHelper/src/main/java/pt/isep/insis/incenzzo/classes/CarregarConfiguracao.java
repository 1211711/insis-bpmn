package pt.isep.insis.incenzzo.classes;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Properties;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pt.isep.insis.incenzzo.listeners.ConvidarPessoasListener;

public class CarregarConfiguracao implements JavaDelegate{
	
	private final String FILE_NAME = "configuracao.properties";
	private final String FILE_PATH = Paths.get("").toAbsolutePath().toString();
	
	private static Log log = LogFactory.getLog(CarregarConfiguracao.class);
			
	@Override
	public void execute(DelegateExecution executionContext) throws Exception {
		
		log.info("Load Configuracao");
		
		log.info(String.format("File Path: %s", FILE_PATH));
		log.info(String.format("File Name: %s", FILE_NAME));
		
		try {
			InputStream input = new FileInputStream(FILE_PATH + "/conf/" + FILE_NAME);
			
			// Carregar ficheiro de configuracao
			Properties config = new Properties();
			config.load(input);
			
			// Listar e guardar configuracoes
			Enumeration<?> keys = config.keys();
			while (keys.hasMoreElements()) {
				String key = String.valueOf(keys.nextElement());
				
				log.info(String.format("Key: %s, Valor: %s", key, config.getProperty(key)));
				executionContext.setVariable(key, config.getProperty(key));
			}			
			
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
	}

}

package pt.isep.insis.incenzzo.classes;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.databind.node.ObjectNode;

public class DefinirConfiguracoes implements JavaDelegate {

	private final String CONFIG = "configuracao";
	private final String TIMEOUT = "timeout";
	private final String TENTATIVAS = "tentativas";
	private final String MAX_CONVIDADOS = "limiteConvidados";

	private static Log log = LogFactory.getLog(DefinirConfiguracoes.class);

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		if (!execution.hasVariable(CONFIG)) {
			return;
		}

		ObjectNode configs = execution.getVariable(CONFIG, ObjectNode.class);

		log.info(String.format("Timeout: %s", configs.get(TIMEOUT)));
		log.info(String.format("Tentativas: %s", configs.get(TENTATIVAS)));
		log.info(String.format("Max Convidados: %s", configs.get(MAX_CONVIDADOS)));

		execution.setVariable(TIMEOUT, configs.get(TIMEOUT).asLong());
		execution.setVariable(TENTATIVAS, configs.get(TENTATIVAS).asLong());
		execution.setVariable(MAX_CONVIDADOS, configs.get(MAX_CONVIDADOS).asLong());
	}

}

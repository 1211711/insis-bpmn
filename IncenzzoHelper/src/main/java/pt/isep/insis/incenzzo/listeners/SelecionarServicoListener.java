package pt.isep.insis.incenzzo.listeners;

import java.io.IOException;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.bpmn.core.types.datatypes.json.JSONUtils;
import org.wso2.carbon.bpmn.core.types.datatypes.json.api.JsonNodeObject;

public class SelecionarServicoListener implements TaskListener {

	private static final String SERVICO_ATUAL = "servicoAtual";
	private static final String SERVICO_SELECIONADO = "servicoSelecionado";

	private static Log log = LogFactory.getLog(SelecionarServicoListener.class);

	@Override
	public void notify(DelegateTask execution) {
		log.info(".... Listener Selecionar Serviço ....");

		String servicoAtual = (execution.hasVariable(SERVICO_ATUAL))
				? execution.getVariable(SERVICO_ATUAL, String.class)
				: "";

		log.info("Servico Atual: " + servicoAtual);

		if (execution.hasVariable(SERVICO_SELECIONADO)
				&& !((List<String>) execution.getVariable(SERVICO_SELECIONADO)).get(0).isEmpty()) {
			try {
				JsonNodeObject servicoSelecionadoObj = JSONUtils
						.parse(((List<String>) execution.getVariable(SERVICO_SELECIONADO)).get(0).toString());
				servicoAtual = servicoSelecionadoObj.jsonPath("$.nome").toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.info("Servico Atual 2: " + servicoAtual);

		execution.setVariable(SERVICO_ATUAL, servicoAtual);
	}

}

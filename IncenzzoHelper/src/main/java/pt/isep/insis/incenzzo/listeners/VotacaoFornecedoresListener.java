package pt.isep.insis.incenzzo.listeners;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class VotacaoFornecedoresListener implements TaskListener {

	private static final long serialVersionUID = 1L;

	private static final String FORNECEDOR = "fornecedor";
	private static final String ACEITA_RESERVA = "aceitaReserva";
	private static final String FORNECEDORES_AFIRMATIVOS = "fornecedoresAfirmativos";

	private static final String LISTA_FORNECEDORES = "fornecedoresIds";
	private static final String LISTA_FORNECEDORES_EMAILS = "fornecedoresEmailsTimeout";

	private static final String TIMEOUT_FORNECEDORES = "timeoutOnceFornecedores";

	private static Log log = LogFactory.getLog(VotacaoFornecedoresListener.class);

	@Override
	public void notify(DelegateTask executionContext) {
		log.info("A recolher resultados da votação de fornecedores...");

		List<String> fornecedoresAfirmativos = (executionContext.hasVariable(FORNECEDORES_AFIRMATIVOS))
				? (List<String>) executionContext.getVariable(FORNECEDORES_AFIRMATIVOS)
				: new ArrayList<String>();

		// Colocamos uma resposta default como sim para colmatar os erros do BPS
		String respostaFornecedor = (executionContext.hasVariable(ACEITA_RESERVA))
				? executionContext.getVariable(ACEITA_RESERVA, String.class)
				: "sim";

		String fornecedor = executionContext.getVariable(FORNECEDOR, String.class);

		log.info("Fornecedor: " + fornecedor + "\tResposta: " + respostaFornecedor);

		if (respostaFornecedor.equals("sim"))
			fornecedoresAfirmativos.add(fornecedor);
		else {
			// Se um fornecedor disser que não os restantes, se não responderem, nem
			// precisam de seguir o processo de timeout
			executionContext.setVariable(TIMEOUT_FORNECEDORES, false);
		}

		executionContext.setVariable(FORNECEDORES_AFIRMATIVOS, fornecedoresAfirmativos);

		// Eliminar email do fornecedor atual (já votou) vvv

		List<String> listaFornecedores = (executionContext.hasVariable(LISTA_FORNECEDORES))
				? (List<String>) executionContext.getVariable(LISTA_FORNECEDORES)
				: new ArrayList<String>();

		List<String> listaEmails = (executionContext.hasVariable(LISTA_FORNECEDORES_EMAILS))
				? (List<String>) executionContext.getVariable(LISTA_FORNECEDORES_EMAILS)
				: new ArrayList<String>();

		int fornecedorIndex = listaFornecedores.indexOf(fornecedor);
		listaFornecedores.remove(fornecedorIndex);
		listaEmails.remove(fornecedorIndex);

		executionContext.setVariable(LISTA_FORNECEDORES, listaFornecedores);
		executionContext.setVariable(LISTA_FORNECEDORES_EMAILS, listaEmails);
	}

}

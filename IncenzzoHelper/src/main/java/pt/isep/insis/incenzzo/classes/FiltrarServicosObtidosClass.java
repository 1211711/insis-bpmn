package pt.isep.insis.incenzzo.classes;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.bpmn.core.types.datatypes.json.JSONUtils;
import org.wso2.carbon.bpmn.core.types.datatypes.json.api.JsonNodeObject;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class FiltrarServicosObtidosClass implements JavaDelegate {

	private static final String SERVICOS = "servicos";

	private static final String SERVICO_PRETENDIDO = "servicoPretendido";

	private static final String SERVICOS_OBTIDOS = "servicosObtidos";

	private static final String MENOR_IDADE = "menorIdade";

	private static final String TODOS_CONVIDADOS = "todosConvidados";

	private static Log log = LogFactory.getLog(FiltrarServicosObtidosClass.class);

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		String servicoPretendido = execution.getVariable(SERVICO_PRETENDIDO, String.class);

		log.info("---- Script Filtrar " + servicoPretendido + " Obtidos ----");

		List<String> todosConvidados;

		if (execution.hasVariable(TODOS_CONVIDADOS)) {
			todosConvidados = (List<String>) execution.getVariable(TODOS_CONVIDADOS);
		} else {
			todosConvidados = new ArrayList<String>();
		}

		JsonNodeObject obj;
		List<String> servicos = new ArrayList<String>();
		List<String> temp = new ArrayList<String>();

		ObjectNode servicosObj = (ObjectNode) execution.getVariable(SERVICOS_OBTIDOS);

		log.info("GetServico X: " + servicosObj.get(servicoPretendido.toLowerCase()) + "is class: "
				+ servicosObj.get(servicoPretendido.toLowerCase()).getClass());

		ArrayNode servicosArrayNode = (ArrayNode) servicosObj.get(servicoPretendido.toLowerCase());

		for (JsonNode jsonNode : servicosArrayNode) {
			log.info("JsonNode: " + jsonNode.toString());
			servicos.add(jsonNode.toString());
		}

		Long menorIdade = (execution.hasVariable(MENOR_IDADE)) ? execution.getVariable(MENOR_IDADE, Long.class) : 21L;

		log.info(servicos);

		for (int j = 0; j < servicos.size(); j++) {
			obj = JSONUtils.parse(servicos.get(j));
			log.info(obj);
			log.info(obj.jsonPath("$.minIdade"));
			if (Long.valueOf(obj.jsonPath("$.minIdade").toString()) <= menorIdade
					&& Long.valueOf(obj.jsonPath("$.maxPessoas").toString()) >= todosConvidados.size()) {
				temp.add(servicos.get(j));
			}
		}
		log.info(temp);

		execution.setVariable(SERVICOS, temp);
	}

}

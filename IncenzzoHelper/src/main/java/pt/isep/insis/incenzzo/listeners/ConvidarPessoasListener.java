package pt.isep.insis.incenzzo.listeners;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ConvidarPessoasListener implements TaskListener {

	private static final String CONVIDADO = "convidado";

	private static final String TODOS_CONVIDADOS = "todosConvidados";
	private static final String EMAILS_CONVIDADOS = "emailsConvidados";
	private static final String MENOR_IDADE = "menorIdade";
	private static final String NOVOS_CONVIDADOS = "novosConvidados";
	private static final String ATUAIS_CONVIDADOS = "atuaisConvidados";

	private static final String CONTINUAR_A_CONVIDAR = "continuarAConvidar";
	private static final String NOME_CONVIDADO = "nomeConvidado";
	private static final String IDADE_CONVIDADO = "idadeConvidado";
	private static final String EMAIL_CONVIDADO = "emailConvidado";

	private static Log log = LogFactory.getLog(ConvidarPessoasListener.class);

	public void notify(DelegateTask executionContext) {
		log.info("A convidar pessoas...");

		List<String> todosConvidados;

		if (executionContext.hasVariable(TODOS_CONVIDADOS)) {
			todosConvidados = (List<String>) executionContext.getVariable(TODOS_CONVIDADOS);
		} else {
			todosConvidados = new ArrayList<String>();
			todosConvidados.add("criador");
		}

		List<String> emailsConvidados;

		if (executionContext.hasVariable(EMAILS_CONVIDADOS)) {
			emailsConvidados = (List<String>) executionContext.getVariable(EMAILS_CONVIDADOS);
		} else {
			emailsConvidados = new ArrayList<String>();
			emailsConvidados.add("1210631@isep.ipp.pt");
		}

		Long menorIdade = (Long) ((executionContext.hasVariable(MENOR_IDADE))
				? executionContext.getVariable(MENOR_IDADE)
				: 22L);

		List<String> novosConvidados;

		if (executionContext.hasVariable(NOVOS_CONVIDADOS)) {
			novosConvidados = (List<String>) executionContext.getVariable(NOVOS_CONVIDADOS);
		} else {
			novosConvidados = new ArrayList<String>();
			novosConvidados.add("criador");
		}

		List<String> atuaisConvidados;
		if (executionContext.hasVariable(ATUAIS_CONVIDADOS)) {
			atuaisConvidados = (List<String>) executionContext.getVariable(ATUAIS_CONVIDADOS);
		} else {
			atuaisConvidados = new ArrayList<String>();
			atuaisConvidados.add("criador");
		}

		String continuarAConvidar = (String) (executionContext.hasVariable(CONTINUAR_A_CONVIDAR)
				? executionContext.getVariable(CONTINUAR_A_CONVIDAR)
				: "sim");
		String nomeConvidado = (String) (executionContext.hasVariable(NOME_CONVIDADO)
				? executionContext.getVariable(NOME_CONVIDADO)
				: null);
		Long idadeConvidado = (Long) (executionContext.hasVariable(IDADE_CONVIDADO)
				? executionContext.getVariable(IDADE_CONVIDADO)
				: null);
		String emailConvidado = (String) ((executionContext.hasVariable(EMAIL_CONVIDADO))
				? executionContext.getVariable(EMAIL_CONVIDADO)
				: null);

		String convidadoAtual = (String) executionContext.getVariable(CONVIDADO);

		if (continuarAConvidar.equals("nao")) {
			// Remover convidado atual das listas: Novos Convidados
			novosConvidados.remove(convidadoAtual);
		}

		if (nomeConvidado != null && !nomeConvidado.isEmpty() && idadeConvidado != null && idadeConvidado > 0L
				&& emailConvidado != null && !emailConvidado.isEmpty()) {
			todosConvidados.add(nomeConvidado);
			emailsConvidados.add(emailConvidado);
			menorIdade = (menorIdade > idadeConvidado) ? idadeConvidado : menorIdade;
			novosConvidados.add(nomeConvidado);
		}

		// Remover convidado atual das lista Atuais Convidados
		atuaisConvidados.remove(convidadoAtual);

		// Print da informação atualizada
		log.info(String.format("%s vai convidar mais? %s", convidadoAtual, continuarAConvidar));

		log.info("----- Novo Convidado -----");
		log.info(String.format("%s (%s) foi convidado por %s", nomeConvidado, idadeConvidado, convidadoAtual));

		log.info("----- Grupo Total -----");
		for (int i = 0; i < todosConvidados.size(); i++) {
			log.info(String.format("Pessoa %d: %s", i + 1, todosConvidados.get(i)));
		}

		executionContext.setVariable(NOME_CONVIDADO, "");
		executionContext.setVariable(IDADE_CONVIDADO, 0);
		executionContext.setVariable(EMAIL_CONVIDADO, "");
		
		executionContext.setVariable(TODOS_CONVIDADOS, todosConvidados);
		executionContext.setVariable(EMAILS_CONVIDADOS, emailsConvidados);
		executionContext.setVariable(MENOR_IDADE, menorIdade);
		executionContext.setVariable(NOVOS_CONVIDADOS, novosConvidados);
		executionContext.setVariable(ATUAIS_CONVIDADOS, atuaisConvidados);
	}

}
